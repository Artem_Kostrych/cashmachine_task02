package com.example.demo.controller;

import com.example.demo.exception.CardNotExistException;
import com.example.demo.exception.LimitExceededException;
import com.example.demo.exception.NotEnoughMoneyException;
import org.springframework.http.HttpStatus;

import com.example.demo.config.response.ResponseMessage;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class ExceptionHandlerController {

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(CardNotExistException.class)
    public ResponseEntity handleCardNotExistException() {
        System.out.println("\"This card does not exist!!\"");
        return new ResponseEntity(new ResponseMessage("This card does not exist!!"), HttpStatus.CONFLICT );
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(LimitExceededException.class)
    public ResponseEntity handleLimitExceededException() {
        return new ResponseEntity(new ResponseMessage("Limit exceeded!!"), HttpStatus.CONFLICT );
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(NotEnoughMoneyException.class)
    public ResponseEntity handleNotEnoughMoneyException() {
        return new ResponseEntity(new ResponseMessage("Not enough money on this card!!"), HttpStatus.CONFLICT );
    }

}
