package com.example.demo.controller;

import com.example.demo.exception.CardNotExistException;
import com.example.demo.model.Transaction;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import com.example.demo.service.impl.security.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private AuthService authService;

    @RequestMapping(value = "/showUsers", method = RequestMethod.GET)
    public List<User> showUsers() {
        return userService.getAllUsers();
    }

    @PostMapping("/signin")
    public ResponseEntity<?> signinUser(@RequestBody User user) {
        return authService.authenticateUser(user);
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ResponseEntity<?> signupUser(@RequestBody User user) {
        return authService.registerUser(user);
    }

}
