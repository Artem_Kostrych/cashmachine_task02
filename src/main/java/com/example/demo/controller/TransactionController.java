package com.example.demo.controller;

import com.example.demo.model.Transaction;
import com.example.demo.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @RequestMapping(value = "/showTransactions", method = RequestMethod.GET)
    public List<Transaction> showTransactions(){
        return transactionService.showAll();
    }

    @RequestMapping(value = "/sendMoney", method = RequestMethod.POST)
    public String sendMoney(@RequestBody Transaction transaction){
        transactionService.sendMoney(transaction);
        return "redirect:/showUsers";
    }

    @RequestMapping(value = "/putMoney", method = RequestMethod.POST)
    public String putMoney(@RequestBody Transaction transaction){
        transactionService.putMoney(transaction);
        return "redirect:/showUsers";
    }

    @RequestMapping(value = "/takeMoney", method = RequestMethod.POST)
    public String takeMoney(@RequestBody Transaction transaction){
        transactionService.takeMoney(transaction);
        return "redirect:/showUsers";
    }
}
