package com.example.demo.model.security;

public enum RoleName {
  ROLE_USER,
  ROLE_ADMIN
}
