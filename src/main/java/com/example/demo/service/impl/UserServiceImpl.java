package com.example.demo.service.impl;

import com.example.demo.dao.UserDao;
import com.example.demo.model.Transaction;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserServiceImpl  implements UserService {

    @Autowired
    private UserDao userDao;

    public boolean existsByUsername(String username){
        return userDao.existsByUsername(username);
    }

    @Override
    public void insert(User user) {
        userDao.save(user);
    }

    @Override
    public List<User> getAllUsers(){
        return userDao.getAll();
    }

    @Override
    public boolean existsByEmail(String email){
        return userDao.existsByEmail(email);
    }
}
