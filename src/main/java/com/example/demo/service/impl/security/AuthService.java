package com.example.demo.service.impl.security;


import com.example.demo.config.JwtTokenProvider;
import com.example.demo.model.User;
import com.example.demo.config.response.JwtResponse;
import com.example.demo.config.response.ResponseMessage;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private JwtTokenProvider jwtProvider;

    @Autowired
    public AuthService(AuthenticationManager authenticationManager, UserService userService,
                       PasswordEncoder encoder, JwtTokenProvider jwtProvider) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
        this.encoder = encoder;
        this.jwtProvider = jwtProvider;
    }


    public ResponseEntity authenticateUser(User user) {
        return getResponseBody(user, getAuthentication(user.getUsername(), user.getPassword()));
    }

    public ResponseEntity registerUser(User user) {
        if (userService.existsByUsername(user.getUsername())) {
            return new ResponseEntity<>(new ResponseMessage("Fail -> Username is already in use!"),
                    HttpStatus.BAD_REQUEST);
        }

        if (userService.existsByEmail(user.getEmail())) {
            return new ResponseEntity<>(new ResponseMessage("Fail -> Email is already in use!"),
                    HttpStatus.BAD_REQUEST);
        }

        final String rawPassword = user.getPassword();
        user.setPassword(encoder.encode(rawPassword));
        userService.insert(user);
        return getResponseBody(user, getAuthentication(user.getUsername(), rawPassword));
    }

    private Authentication getAuthentication(final String username, final String password) {
        return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    }

    private ResponseEntity getResponseBody(User user, Authentication authentication) {

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtProvider.generateToken(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities()));
    }
}