package com.example.demo.service.impl.security;

import com.example.demo.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserDao userDao;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String usernameOrEmail) {
        return  userDao.findUserByUsernameOrEmail(usernameOrEmail, usernameOrEmail);
    }

    @Transactional
    public UserDetails loadUserById(Long id) {
        return userDao.findById(id).get();
    }
}
