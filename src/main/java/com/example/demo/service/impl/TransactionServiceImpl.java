package com.example.demo.service.impl;

import com.example.demo.dao.TransactionDao;
import com.example.demo.dao.UserDao;
import com.example.demo.exception.CardNotExistException;
import com.example.demo.exception.LimitExceededException;
import com.example.demo.exception.NotEnoughMoneyException;
import com.example.demo.model.Transaction;
import com.example.demo.model.User;
import com.example.demo.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionDao transactionDao;

    @Autowired
    private UserDao userDao;

    @Transactional
    public void sendMoney(Transaction transaction){
        User sender =userDao.findUserByCardNumber(transaction.getSender());
        if(sender==null)
            throw new CardNotExistException(transaction.getSender());
        if(sender.getBalance().compareTo(transaction.getAmount())<0) {
            throw new NotEnoughMoneyException(sender.getCardNumber());
        }
        if (sender.getMax()!= null && transaction.getAmount().compareTo(sender.getMax()) > 0) {
            throw new LimitExceededException(sender.getMax().toString());
        }
        User receiver = userDao.findUserByCardNumber(transaction.getReceiver());
        if(receiver==null) {
            throw new CardNotExistException(transaction.getReceiver());
        }
        userDao.updateUser(sender.getUsername(), sender.getBalance().subtract(transaction.getAmount()));
        userDao.updateUser(receiver.getUsername(), receiver.getBalance().add(transaction.getAmount()));
        transactionDao.save(transaction);
    }

    @Transactional
    public void putMoney(Transaction transaction){
        User receiver = userDao.findUserByCardNumber(transaction.getReceiver());
        if(receiver==null){
            throw new CardNotExistException(transaction.getReceiver());
        }
        userDao.updateUser(receiver.getUsername(), receiver.getBalance().add(transaction.getAmount()));
        transactionDao.save(transaction);
    }

    @Transactional
    public void takeMoney(Transaction transaction){
        User sender =userDao.findUserByCardNumber(transaction.getSender());
        if(sender==null)
            throw new CardNotExistException(transaction.getSender());
        if(sender.getBalance().compareTo(transaction.getAmount())<0) {
            throw new NotEnoughMoneyException(sender.getCardNumber());
        }
        if (sender.getMax()!= null && transaction.getAmount().compareTo(sender.getMax()) > 0) {
            throw new LimitExceededException(sender.getMax().toString());
        }
        userDao.updateUser(sender.getUsername(), sender.getBalance().subtract(transaction.getAmount()));
        transactionDao.save(transaction);
    }

    @Override
    public List<Transaction> showAll() {
        return transactionDao.getAll();
    }
}
