package com.example.demo.service;

import com.example.demo.model.Transaction;

import java.util.List;

public interface TransactionService {
    void putMoney(Transaction transaction);
    void sendMoney(Transaction transaction);
    void takeMoney(Transaction transaction);
    List<Transaction> showAll();
}
