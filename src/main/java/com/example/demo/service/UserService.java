package com.example.demo.service;

import com.example.demo.model.Transaction;
import com.example.demo.model.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();
    boolean existsByEmail(String email);
    boolean existsByUsername(String email);
    void insert(User user);
}
