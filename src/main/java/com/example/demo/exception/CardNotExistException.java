package com.example.demo.exception;

public class CardNotExistException extends RuntimeException {
    public CardNotExistException(String errorMessage) {
        super(errorMessage);
    }
}
