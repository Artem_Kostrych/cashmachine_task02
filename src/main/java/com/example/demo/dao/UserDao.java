package com.example.demo.dao;

import com.example.demo.model.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Repository
public interface UserDao extends CrudRepository<User, Long>, GenericDao<User, Long> {


    @Modifying
    @Query(value = "select * from USER;",
            nativeQuery = true)
    @Transactional
    List<User> getAll();

    User findUserByCardNumber(String cardNumber);

    boolean existsByUsername(String username);

    boolean existsByEmail(String email);

    User findByUsername(String username);

    User findUserByUsernameOrEmail(String username, String email);

    @Modifying
    @Query(value = "update user set balance = :balance where username = :username",
            nativeQuery = true)
    @Transactional
    void updateUser(@Param("username") String username,
                    @Param("balance") BigDecimal balance);
}
