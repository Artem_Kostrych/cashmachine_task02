package com.example.demo.dao;

import com.example.demo.model.Transaction;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface TransactionDao extends CrudRepository<Transaction, Long>, GenericDao<Transaction, Long> {

    @Modifying
    @Query(value = "select * from transaction ;",
            nativeQuery = true)
    @Transactional
    List<Transaction> getAll();
}
